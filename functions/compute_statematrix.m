function stateCollectMat = ...
    compute_statematrix(inputSequence, outputSequence, esn, nForgetPoints, varargin)
% Copyright: Fraunhofer IAIS 2006 / Patent pending

if isempty(inputSequence) && isempty(outputSequence)
    error('error in compute_statematrix: two empty input args');
end

if isempty(outputSequence)
    teacherForcing = 0;
    nDataPoints = length(inputSequence(:,1));
else
    teacherForcing = 1;
    nDataPoints = length(outputSequence(:,1));
end

if nForgetPoints >= 0
    stateCollectMat = ...
        zeros(nDataPoints - nForgetPoints, esn.nInputUnits + esn.nInternalUnits) ; 
else
    stateCollectMat = ...
        zeros(nDataPoints, esn.nInputUnits + esn.nInternalUnits) ; 
end

%% set starting state
totalstate = zeros(esn.nInputUnits + esn.nInternalUnits + esn.nOutputUnits, 1);

collectIndex = 0;
for i = 1:nDataPoints
    
    % scale and shift the value of the inputSequence
    if esn.nInputUnits > 0
        in = esn.inputScaling .* inputSequence(i,:)' + esn.inputShift;  % in is column vector
    else in = [];
    end
    
    % write input into totalstate
    if esn.nInputUnits > 0
        totalstate(esn.nInternalUnits+1:esn.nInternalUnits + esn.nInputUnits) = in;
    end    
    
    internalState =  tanh([esn.internalWeights , esn.inputWeights , esn.feedbackWeights * diag(esn.feedbackScaling )] * totalstate)  ;   
    internalState = internalState + esn.noiseLevel * (rand(esn.nInternalUnits,1) - 0.5) ; 

    if teacherForcing
        netOut = esn.teacherScaling .* outputSequence(i,:)' + esn.teacherShift;
    else        
        netOut = esn.outputWeights * [internalState; in];
    end
    
    totalstate = [internalState; in; netOut];
    
    if nForgetPoints >= 0 &&  i > nForgetPoints
        collectIndex = collectIndex + 1;
        stateCollectMat(collectIndex,:) = [internalState' in']; 
    elseif nForgetPoints < 0
        collectIndex = collectIndex + 1;
        stateCollectMat(collectIndex,:) = [internalState' in']; 
    end
    
end
