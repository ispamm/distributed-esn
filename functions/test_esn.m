function [testError, esn_output, teacher_output, stateCollection] = test_esn(inputSequence, outputSequence, esn, config)
% TEST_ESN - Test a trained ESN
% Inputs:
%   - inputSequence: test input sequences.
%   - outputSequence: test output sequences.
%   - esn: ESN struct.
%   - config: configuration struct created in config.m.

% Compute the states
stateCollection = gather_states( inputSequence, [], esn, config.dropout );

% Compute the predicted output
esn_output = stateCollection * esn.outputWeights' ;

% Remove dropout elements
nOutputPoints = length(esn_output(:,1)) ;
esn_output = esn_output - repmat(esn.teacherShift',[nOutputPoints 1]) ;
esn_output = esn_output / diag(esn.teacherScaling) ;

% Gather the teacher signal
teacher_output = gather_teacher( outputSequence, esn, config.dropout, false );

% Compute the error
testError = config.err_fcn(esn_output, teacher_output);