function [ error_local, error_cons, training_time_local, training_time_cons, admm_info] = ...
    train_local_esns(trainInfo, testInputSequence, testOutputSequence, topology, esn, config)
% TRAIN_LOCAL_ESN - Train L-ESN and ADMM-ESN sequentially
% Inputs:
%   - trainInfo: a structure with the training information of C-ESN (see
%   train_esn.m). This contains the state and teacher matrices, so as to use
%   the exact partitioning of C-ESN.
%   - testInputSequence: the test input sequences
%   - testOutputSequence: the test output sequences
%   - topology: the NetworkTopology object describing the current network
%   topology.
%   - esn: ESN structure created using generate_esn.m.
%   - config: the configuration struct defined in config.m.
% Outputs:
%   - error_local: error of L-ESN.
%   - error_cons: error of ADMM-ESN.
%   - training_time_local: training time of L-ESN.
%   - training_time_cons: training time of ADMM-ESN.
%   - admm_info: structure with information on the ADMM procedure
%   (particularly the evolution of the residuals).

% Recover the centralized training information
stateCollection = trainInfo.stateCollection;
teacherCollection = trainInfo.teacherCollection;

% Initialize output structures
error_local = 0;
training_time_local = trainInfo.gathering_time;

% Split the data across the nodes
cv_local = cvpartition(size(stateCollection, 1), 'K', topology.N);

for n = 1:topology.N
    
    stateTemp_local = stateCollection(cv_local.test(n), :);
    teacherTemp_local = teacherCollection(cv_local.test(n), :);
    
    % Training step
    tic;
    esn.outputWeights = (stateTemp_local'*stateTemp_local + config.lambda*eye(esn.nInputUnits + esn.nInternalUnits))\stateTemp_local'*teacherTemp_local;
    esn.outputWeights = esn.outputWeights';
    training_time_local = training_time_local + toc;
    
    error_local = error_local + test_esn(testInputSequence, testOutputSequence, esn, config);
    
end

error_local = error_local/topology.N;
training_time_local = training_time_local/topology.N;

tic;
z = zeros(esn.nInputUnits + esn.nInternalUnits, 1);
u = zeros(esn.nInputUnits + esn.nInternalUnits, topology.N);

% Parameters
rho = config.admm_rho;
steps = config.admm_iters;
N_hidden = esn.nInputUnits + esn.nInternalUnits;

% Statistics initialization
admm_info.r_norm = zeros(steps, 1);
admm_info.s_norm = zeros(steps, 1);
admm_info.eps_pri = zeros(steps, 1);
admm_info.eps_dual = zeros(steps, 1);

% Precompute the inverse matrices
Hinv = cell(topology.N, 1);
HY = cell(topology.N, 1);
for ii = 1:topology.N
    
    stateTemp_local = stateCollection(cv_local.test(ii), :);
    teacherTemp_local = teacherCollection(cv_local.test(ii), :);
    
    HY{ii} = stateTemp_local'*teacherTemp_local;
    
    if(size(stateTemp_local, 1) > N_hidden)
        Hinv{ii} = inv(eye(N_hidden)*rho + stateTemp_local' * stateTemp_local);
    else
        Hinv{ii} = (1/rho)*(eye(N_hidden) - stateTemp_local'/(rho*eye(size(stateTemp_local, 1)) + stateTemp_local*stateTemp_local')*stateTemp_local);
    end
    
end

beta = zeros(N_hidden, topology.N);

for ii = 1:steps
    
    for jj = 1:topology.N
        
        % Compute current weights
        beta(:, jj) = Hinv{jj}*(HY{jj} + rho*z - rho*u(:, jj));
        
    end
    
    % Run consensus
    [beta_avg, tmp1] = ...
        consensus(beta, config.consensus_iterations, config.consensus_threshold, topology);
    [u_avg, tmp2] = consensus(u, config.consensus_iterations, config.consensus_threshold, topology);
    
    % Save statistic
    admm_info.consensus_steps(ii) = max(sum(tmp1 ~= 0), sum(tmp2 ~=0));
    
    % Store the old z and update it
    zold = z;
    z = rho*(beta_avg + u_avg)/(config.lambda/topology.N + rho);
    
    % Compute the update for the Lagrangian multipliers
    for jj = 1:topology.N
        u(:, jj) = u(:, jj) + beta(:, jj) - z;
    end
    
    % Compute primal and dual residuals
    for jj = 1:topology.N
        
        newbeta = beta(:, jj);
        newt = u(:, jj)/rho;
        
        admm_info.r_norm(ii) = admm_info.r_norm(ii) + ...
            norm(newbeta - z, 'fro');
        % Compute epsilon values
        admm_info.eps_pri(ii) = admm_info.eps_pri(ii) + ...
            sqrt(topology.N)*config.admm_abstol + ...
            config.admm_reltol*max(norm(newbeta, 'fro'), norm(z, 'fro'));
        
        admm_info.eps_dual(ii)= admm_info.eps_dual(ii) + ...
            sqrt(topology.N)*config.admm_abstol + ...
            config.admm_reltol*norm(newt, 'fro');
    end
    
    admm_info.r_norm(ii) = admm_info.r_norm(ii)/topology.N;
    admm_info.eps_pri(ii) = admm_info.eps_pri(ii)/topology.N;
    admm_info.eps_dual(ii) = admm_info.eps_dual(ii)/topology.N;
    admm_info.s_norm(ii) = norm(-rho*(z - zold), 'fro');
    
    if(mod(ii, 25) == 0)
        fprintf('\t\tADMM iteration #%i: primal residual = %.2f, dual_residual = %.2f.\n', ...
                ii, admm_info.r_norm(ii), admm_info.s_norm(ii));
    end  
    
    if(admm_info.r_norm(ii) < admm_info.eps_pri(ii) && ...
            admm_info.s_norm(ii) < admm_info.eps_dual(ii))
        break;
    end
    
end

esn.outputWeights = z';
training_time_cons = (trainInfo.gathering_time + toc)/topology.N;

error_cons = test_esn(testInputSequence, testOutputSequence, esn, config);

end

