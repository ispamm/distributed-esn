function [final_value, consensus_error] = consensus(initial_values, steps, threshold, topology)
% CONSENSUS - Run the consensus procedure.
% Inputs:
%   - initial_values: matrix of initial values. For vectors, the size is
%   DxN, where D is the size of the vector and N the number of nodes in the
%   network. For matrices, the size is TxDxN, where TxD is the size of the
%   matrix.
%   - steps: maximum number of iterations.
%   - threshold: termintion criterion to stop the consensus procedure.
%   - topology: NetworkTopology object describing the network.
% Outputs:
%   - final_value: final value after consensus.
%   - consensus_error: vector of errors (one value for each iteration).

if(~topology.isConnected())
    error('The network topology is not connected. Have you initialized it correctly?');
end

current_values = initial_values;
consensus_error = zeros(steps, 1);
is_matrix = ndims(initial_values) == 3;

max_degree = topology.getMaxDegree();

for ii = 1:steps
    new_values = current_values;
    for jj = 1:topology.N
        idx = topology.getNeighbors(jj);
        if(is_matrix)
            new_values(:, :, jj) = ((1-length(idx)/(max_degree+1))*current_values(:, :, jj) + (1/(max_degree + 1))*sum(current_values(:, :, idx), 3));
            consensus_error(ii) = consensus_error(ii) + norm(current_values(:, :, jj) - new_values(:, :, jj), 'fro');
        else
            new_values(:, jj) = ((1-length(idx)/(max_degree+1))*current_values(:, jj) + (1/(max_degree + 1))*sum(current_values(:, idx), 2));
            consensus_error(ii) = consensus_error(ii) + norm(current_values(:, jj) - new_values(:, jj));
        end
    end
    
    consensus_error(ii) = consensus_error(ii)./topology.N;
    current_values = new_values;
    
    if(consensus_error(ii) < threshold)
        break;
    end
end

if(is_matrix)
    final_value = current_values(:, :, 1);
else
    final_value = current_values(:, 1);
end


end