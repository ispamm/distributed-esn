% FORMAT_OUTPUT - Print statistics for the simulation on the console

% Compute average error
% error_mean_dist is a vector containing the average for every network
% configuration.
error_mean_cesn = mean(errors(1, 1, :));
error_mean_dist = mean(errors(2:3, :, :), 3);

% Compuute standard deviation for the error (same as before)
error_std_cesn = std(errors(1, 1, :), 1, 3);
error_std_dist = std(errors(2:3, :, :), 1, 3);

% Compute average training time (same as before)
time_mean_cesn = mean(times(1, 1, :), 3);
time_mean_dist = mean(times(2:3, :, :), 3);

% Compute standard deviation for the training time (same as before)
time_std_cesn = std(times(1, 1, :), 1, 3);
time_std_dist = std(times(2:3, :, :), 1, 3);

nodes_tobetested = length(config.nodes_range);

fprintf('\n\nDetails of the simulation:\n---------------------------------------------------------------------------------------------------------------\n');

% Stores the row labels (i.e. the algorithms' names)
row_labels = cell(nodes_tobetested*2 + 1, 1);
row_labels{1} = 'C-ESN';
w = 1;
for ii = 2:nodes_tobetested + 1
    row_labels{ii} = sprintf('L-ESN (%i nodes): ',  config.nodes_range(w));
    w = w + 1;
end

w = 1;
for ii = ii+1:nodes_tobetested*2+1
    row_labels{ii} = sprintf('ADMM-ESN (%i nodes): ',  config.nodes_range(w));
    w = w + 1;
end

% Print information
disptable([[error_mean_cesn; error_mean_dist(1, :)'; error_mean_dist(2, :)'] [error_std_cesn; error_std_dist(1, :)'; error_std_dist(2, :)']], {'Test error:', 'St.Dev.:'}, row_labels);
disptable([[time_mean_cesn; time_mean_dist(1, :)'; time_mean_dist(2, :)'] [time_std_cesn; time_std_dist(1, :)'; time_std_dist(2, :)']], {'Training time:', 'St.Dev.:'}, row_labels);