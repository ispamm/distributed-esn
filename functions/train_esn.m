function [ error, training_time, trainingInfo ] = train_esn(trainInputSequence, trainOutputSequence, testInputSequence, testOutputSequence, esn, config )
% TRAIN_ESN - Train the centralized ESN (C-ESN)
% Inputs:
%   - trainInputSequence: train input sequences.
%   - trainOutputSequence: train output sequences.
%   - testInputSequence: test input sequences.
%   - testOutputSequence: test output sequence.
%   - esn: ESN struct created with generate_esn.m.
%   - config: struct with configuration parameters created in config.m.
% Outputs:
%   - error: error of C-ESN.
%   - training_time: training time of C-ESN.
%   - trainingInfo: struct with training information, i.e. the state and
%   teacher matrices, the time required to compute them, the final ESN,
%   etc.

dropout = config.dropout;

% Compute state and teacher matrices
tic;
stateCollection = gather_states(trainInputSequence, trainOutputSequence, esn, dropout);
teacherCollection = gather_teacher(trainOutputSequence, esn, dropout, true);
trainingInfo.gathering_time = toc;
training_time = trainingInfo.gathering_time;

% Compute the output matrix
tic;
esn.outputWeights = (stateCollection'*stateCollection + config.lambda*eye(esn.nInputUnits + esn.nInternalUnits))\stateCollection'*teacherCollection;
esn.outputWeights = esn.outputWeights';
training_time = training_time + toc;

% Test the ESN
error = test_esn(testInputSequence, testOutputSequence, esn, config);
    
trainingInfo.stateCollection = stateCollection;
trainingInfo.teacherCollection = teacherCollection;
trainingInfo.esn = esn;

end

