% RUN_SIMULATION - Main simulation file
% Change values in `config.m' to modify the parameters of the run

% Clear the environment
clear all; close all; clc;

% Initialize the PNRG
rng(1);

% Adds the folders to the path
folds = addpath(genpath(pwd));

% Load configuration
fprintf('Loading configuration and data...\n');
config_simulation;

% This is used for datasets that provide their own training/test split
if(exist('X_test', 'var'))
    config.K = 1;
    N_in = size(X_train{1}, 2);
    N_out = size(Y_train{1}, 2);
else
    N_samples = length(Y);
    N_in = size(X{1}, 2);
    N_out = size(Y{1}, 2);
end

% Define output structures
nodes_tobetested = length(config.nodes_range);
errors = zeros(3, nodes_tobetested, config.n_runs*config.K);
times = zeros(3, nodes_tobetested, config.n_runs*config.K);
z = 1;

% Initialize the topologies of the network
topologies = cell(nodes_tobetested);
for ii = 1:nodes_tobetested
    topologies{ii} = RandomTopology(config.nodes_range(ii), config.network_connectivity);
    topologies{ii} = topologies{ii}.initialize();
end

for r_ii = 1:config.n_runs
    
    fprintf('--- RUN %i / %i ---\n', r_ii, config.n_runs);
    
    % Partition the data for the current run
    if(~exist('X_test', 'var'))
        c = cvpartition(N_samples, 'kfold', config.K);
    end

    % Initialize the ESN structure
    esn_o = generate_esn(N_in, config.reservoir_size, N_out, ...
        'spectralRadius', config.desired_specrad, 'inputScaling', repmat(config.input_scaling, N_in, 1) ,'inputShift', repmat(config.input_shift, N_in, 1), ...
        'teacherScaling', repmat(config.teacher_scaling, N_out, 1), 'teacherShift', repmat(config.teacher_shift, N_out, 1), 'feedbackScaling', repmat(config.feedback_scaling, N_out, 1), ...
        'noiseLevel', config.noise_level, 'connectivity', config.connectivity);
    
    for fold = 1:config.K
        
        % Get the data for the current fold
        if(config.K > 1)
            X_train = X(c.training(fold));
            Y_train = Y(c.training(fold));
            X_test = X(c.test(fold));
            Y_test = Y(c.test(fold));
        end
        
        % Print information on screen
        fprintf('Running fold %i of %i (%i training, %i test)\n', fold, config.K, length(X_train), length(X_test));
        statusbar(0, '%i experiments left', config.n_runs*config.K - z);
        
        % Train C-ESN
        fprintf('\tTraining C-ESN... ');
        [errors(1, 1, z), times(1, 1, z), trainInfo] = train_esn(X_train, Y_train, X_test, Y_test, esn_o, config);
        esn = trainInfo.esn;
        fprintf('Final error: %.2f\n', errors(1, 1, z));
        
        for nodes_idx = 1:nodes_tobetested
            
            % Current amount of nodes
            N_nodes = config.nodes_range(nodes_idx);

            if(~config.train_only_cesn)

                % Train L-ESN and ADMM-ESN
                fprintf('\tTraining distributed ESN with %i nodes...\n', N_nodes);
                [errors(2, nodes_idx, z), errors(3, nodes_idx, z), times(2, nodes_idx, z), times(3, nodes_idx, z), admm_info] = ...
                   train_local_esns(trainInfo, X_test, Y_test, topologies{nodes_idx}, esn_o, config);
                fprintf('\tFinal error ADMM-ESN: %.2f\n', errors(3, nodes_idx, z));
            
            end
            
        end
        
        z = z + 1;
        
    end
    
end

% Run the C-ESN a final time (to plot output and states)
[~, predictedTestOutput, teacherCollection, stateMatrix] = test_esn(X_test, Y_test, esn, config);

% Print the results on screen
format_output;

% Plot information
make_plots;

% Save the main results of the simulation
save('Simulation_results.mat', 'config', 'errors', 'times');