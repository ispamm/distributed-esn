% CONFIG_SIMULATION - Define the configuration for the current run

% -------------------------------------------------------------------------
% --- ESN parameters ------------------------------------------------------
% -------------------------------------------------------------------------
config.reservoir_size = 300;       % Size of the reservoir
config.desired_specrad = 0.9;      % Spectral radius of the reservoir
config.feedback_scaling = 0.3;     % Feedback scaling (set to 0 for no feedback)
config.input_scaling = 0.5;        % Input scaling
config.input_shift = 0;            % Input shift
config.teacher_scaling = 0.1;      % Teacher scaling
config.teacher_shift = 0;          % Teacher shift
config.connectivity = 0.25;        % Percentage of non-zero connections in the reservoir 
config.noise_level = 0.0001;       % Variance of noise during state update
config.lambda = 0.001;             % Regularization parameter

% -------------------------------------------------------------------------
% --- Dataset -------------------------------------------------------------
% -------------------------------------------------------------------------
[X, Y] = narma10(2000, 50);                 % NARMA-10 dataset
%[X, Y] = ext_poly(2000, 50, 8, 8);         % Extended polynomial
%[X, Y] = mackeyglass(2000, 50, 17, 10);    % Mackey-Glass time-series
%[X, Y] = lorenz(2000, 50, 1);              % Lorenz time-series


% -------------------------------------------------------------------------
% --- Network and ADMM ----------------------------------------------------
% -------------------------------------------------------------------------
config.nodes_range = 4:2:16;            % Number of nodes in the network
                                        % Simulations are repeated for
                                        % every value in this range
config.network_connectivity = 0.25;     % Connectivity of the network
config.admm_iters = 400;                % Iterations of ADMM
config.admm_rho = 0.01;                 % Regularization parameter of ADMM
config.admm_reltol = 10^-8;             % Relative tolerance value
config.admm_abstol = 10^-8;             % Absolute tolerance value
config.consensus_iterations = 300;      % Iterations for the consensus
config.consensus_threshold = 10^-6;     % Termination threshold for the consensus
config.train_only_cesn = false;         % If `true', train only C-ESN (useful for debugging)

% -------------------------------------------------------------------------
% --- Other ---------------------------------------------------------------
% -------------------------------------------------------------------------
config.dropout = 100;          % Number of dropout elements
config.K = 3;                  % Number of folds for the k-fold cross-validation
config.n_runs = 10;            % Repetitions of the simulation

% -------------------------------------------------------------------------
% --- Error measure -------------------------------------------------------
% -------------------------------------------------------------------------
config.err_fcn = @nrmse;    % Normalized Root Mean-Squared Error
%config.err_fcn = @mse;     % Mean-Squared Error
%config.err_fcn = @msedb;   % Mean-Squared Error in decibels
%config.err_fcn = @nsr;     % Noise-to-signal ratio