function [inputSequences, outputSequences] = narma10(sequenceLength, n_sequences)
% NARMA10 - NARMA-10 time-series, as described in:
%   [1] Jaeger, H. (2002). Adaptive nonlinear system identification with 
%   echo state networks. In Advances in neural information processing systems (pp. 593-600).
% Inputs:
%   - sequenceLenght: length of each sequence
%   - n_sequences: number of sequences to be generated
% Outputs:
%   - inputSequences: cell array of input sequences
%   - outputSequences: cell array of output sequences

inputSequences = cell(n_sequences, 1);
outputSequences = cell(n_sequences, 1);

for n = 1:n_sequences

    inputSequences{n} = [ones(sequenceLength,1) rand(sequenceLength,1)*0.5];
    outputSequences{n} = 0.1*ones(sequenceLength, 1); 

    for i = 11 : sequenceLength
        outputSequences{n}(i, 1) = 0.3*outputSequences{n}(i-1, 1) + 0.05*outputSequences{n}(i-1, 1)*sum(outputSequences{n}(i-10:i-1, 1)) + 1.5*inputSequences{n}(i-9, 2)*inputSequences{n}(i, 2);  
    end
    
    outputSequences{n} = tanh(outputSequences{n} - mean(outputSequences{n}));
    
end

