function [inputSequences, outputSequences] = lorenz(sequenceLength, n_sequences, time_horizon)
% LORENZ - Standard Lorenz chaotic time series.
% Inputs:
%   - sequenceLenght: length of each sequence
%   - n_sequences: number of sequences to be generated
%   - time_horizon: time horizon for the output sequence.
% Outputs:
%   - inputSequences: cell array of input sequences
%   - outputSequences: cell array of output sequences

inputSequences = cell(n_sequences, 1);
outputSequences = cell(n_sequences, 1);

fprintf('Generating Lorenz time-series...');
textprogressbar(' ');

for n = 1:n_sequences

    textprogressbar(n*100/n_sequences);
    [x, y, z] = gen_lorenz(28, 10, 8/3, [rand()*0.1 1 (1 + rand()*0.1)], 0:sequenceLength, 0.001);
      
    inputSequences{n} = [x y z];
    
    % Map to [-1 +1]
    inputSequences{n} = mapminmax(inputSequences{n}', -1, +1);
    inputSequences{n} = inputSequences{n}';
    
    % Compute the output vector
    outputSequences{n} = inputSequences{n}(time_horizon+1:end, 1); 
    
    N_points = length(outputSequences{n});

    % Add unitary input
    inputSequences{n} = [ones(N_points, 1) inputSequences{n}(1:N_points, :)];
 
end

textprogressbar('Done');
