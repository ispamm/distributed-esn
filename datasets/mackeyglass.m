function [inputSequences, outputSequences] = mackeyglass(sequenceLength, n_sequences, tau, time_horizon)
% MACKEYGLASS - Standard Mackey-Glass chaotic time series.
% Inputs:
%   - sequenceLenght: length of each sequence
%   - n_sequences: number of sequences to be generated
%   - tau: memory delay in the recurrent equation. The series is chaotic
%   for tau > 16.8.
%   - time_horizon: time horizon for the output sequence.
% Outputs:
%   - inputSequences: cell array of input sequences
%   - outputSequences: cell array of output sequences

if(nargin < 4)
    time_horizon = 1;
end

inputSequences = cell(n_sequences, 1);
outputSequences = cell(n_sequences, 1);

for n = 1:n_sequences

    % Generate the series with an integration step of 0.1
    inputSequences{n} = gen_mackeyglass(sequenceLength*10, tau, rand()*0.2 + 1);
    
    % Subsample the resulting series
    inputSequences{n} = inputSequences{n}(1:10:end);
    
    % Squash the time-series in [-1 +1]
    inputSequences{n} = tanh(inputSequences{n} - mean(inputSequences{n}));
    
    % Compute the output vector
    outputSequences{n} = inputSequences{n}(time_horizon+1:end); 

    % Compute the final number of points
    N_points = length(outputSequences{n});
    
    % Added unitary input
    inputSequences{n} = [ones(N_points, 1) inputSequences{n}(1:N_points)];
 
end

