function [inputSequences, outputSequences] = narma_simple(sequenceLength, n_sequences, memoryLength)
% NARMA_SIMPLE - A simple NARMA time-series
% Inputs:
%   - sequenceLenght: length of each sequence
%   - n_sequences: number of sequences to be generated
%   - memoryLength: memory of the time-sequence
% Outputs:
%   - inputSequences: cell array of input sequences
%   - outputSequences: cell array of output sequences

inputSequences = cell(n_sequences, 1);
outputSequences = cell(n_sequences, 1);

for n = 1:n_sequences

    inputSequences{n} = [ones(sequenceLength,1) rand(sequenceLength,1)];
    outputSequences{n} = 0.1*ones(sequenceLength, 1); 

    for i = memoryLength + 1 : sequenceLength
        outputSequences{n}(i, 1) = 0.7 * inputSequences{n}(i-memoryLength,2) + 0.1 ...
            + (1 - outputSequences{n}(i-1,1)) * outputSequences{n}(i-1, 1);
    end
    
    outputSequences{n} = tanh(outputSequences{n} - mean(outputSequences{n}));
    
end

