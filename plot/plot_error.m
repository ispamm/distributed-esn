% PLOT_ERROR - Plot the average testing error and training time for the 
% various network configurations

figure('name', 'Testing error');
figshift;

plot([config.nodes_range(1)-1 config.nodes_range(end)+1], [error_mean_cesn, error_mean_cesn], 'k--', 'LineWidth', line_width);
hold on
plot(config.nodes_range, error_mean_dist(1, :), 'r--' ,'LineWidth', line_width);
plot(config.nodes_range, error_mean_dist(2, :), 'r', 'LineWidth', line_width);

xlim([config.nodes_range(1)-1 config.nodes_range(end)+1]);

box on;
grid on;

xlabel('Number of nodes', 'FontSize', font_size, 'FontName', font_name);
ylabel('Error', 'FontSize', font_size, 'FontName', font_name);

h_legend = legend('C-ESN', 'L-ESN', 'ADMM-ESN', 'Location', 'NorthEast');

singlecolumn_format;


figure('name', 'Training time');
figshift;

plot([config.nodes_range(1)-1 config.nodes_range(end)+1], [time_mean_cesn, time_mean_cesn], 'k--', 'LineWidth', line_width);
hold on
plot(config.nodes_range, time_mean_dist(1, :), 'r--' ,'LineWidth', line_width);
plot(config.nodes_range, time_mean_dist(2, :), 'r', 'LineWidth', line_width);

xlim([config.nodes_range(1)-1 config.nodes_range(end)+1]);

box on;
grid on;

xlabel('Number of nodes', 'FontSize', font_size, 'FontName', font_name);
ylabel('Time [secs]', 'FontSize', font_size, 'FontName', font_name);

h_legend = legend('C-ESN', 'L-ESN', 'ADMM-ESN', 'Location', 'NorthEast');

singlecolumn_format;