% MAKE_PLOTS - Plot information for the current simulation

% General properties for the figures
width = 3.45;                   % Width in inches
height = 2.6;                   % Height in inches
font_size = 8;                  % Fontsize
font_size_leg = 6;              % Font size (legend)
font_name = 'TimesNewRoman';    % Font name
line_width = 1;                 % LineWidth

if(~config.train_only_cesn)
    % Plot an example of network
    topologies{1}.visualize();
    figshift;
end

% Plot outputs
plot_outputs;
figshift;

% Plot internal states
plot_internal_states;
figshift;

% Eventually plot the error
if(~config.train_only_cesn)
   plot_error; 
end

% Eventually plot information on ADMM
if(~config.train_only_cesn)
   plot_admm_info; 
end