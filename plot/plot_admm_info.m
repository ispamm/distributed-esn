% PLOT_ADMM_INFO - Plot residuals of ADMM

figure('name', 'Residuals');

plot(1:config.admm_iters, admm_info.r_norm, 'k','LineWidth', line_width);
hold on;
plot(1:config.admm_iters, admm_info.s_norm, 'r--','LineWidth', line_width);

box on;
grid on;

xlabel('ADMM Iteration', 'FontSize', font_size, 'FontName', font_name);
ylabel('Value', 'FontSize', font_size, 'FontName', font_name);

h_legend = legend('Primal', 'Dual', 'Location', 'NorthWest');
singlecolumn_format;