% PLOT_OUTPUTS - Plot outputs of C-ESN

figure('name', 'C-ESN outputs');

% Number of points to be plotted
n_points = 100;

plot(1:n_points, teacherCollection(1:n_points), 'k','LineWidth', line_width);
hold on;
plot(1:n_points, predictedTestOutput(1:n_points), 'r--','LineWidth', line_width);

box on;
grid on;

xlabel('Sample', 'FontSize', font_size, 'FontName', font_name);
ylabel('Value', 'FontSize', font_size, 'FontName', font_name);

h_legend = legend('Real', 'Predicted', 'Location', 'NorthWest');
singlecolumn_format;