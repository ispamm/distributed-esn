% FIND_OPTIMAL_PARAMETERS - Searches the optimal parameters
%   You can use this script to search for the optimal parameters of the ESN
%   on an independent validation set.

% Ranges for every parameter
reservoir_range = [300];                % Reservoir size
specrad_range = [0.1 0.3 0.5 0.7 0.9];  % Spectral radius
feed_range = [0 0.1 0.3 0.5 0.7];       % Feedback range
input_range = [0.1 0.3 0.5 0.7 0.9];    % Input range
teach_range = [0.1 0.3 0.5 0.7 0.9];    % Teacher scaling
lambda_range = 0.001;                   % Regularization parameter
noise_range = [0.0001];                 % Noise term

clear all; close all; clc;
rng(2); % Important: this must be different from the one in 'run_simulation'
folds = addpath(genpath(pwd));

% Compute all the possible combinations
params = combvec(reservoir_range, specrad_range, feed_range, input_range, teach_range, lambda_range, noise_range);

% Load the dataset
config_simulation;

if(exist('X_test', 'var'))
    N_X = size(X_train{1}, 1);
    X = {X_train{1}(1:floor(N_X*0.33), :), X_train{1}(floor(N_X*0.33)+1:floor(N_X*0.66), :), X_train{1}(floor(N_X*0.66)+1:end, :)};
    Y = {Y_train{1}(1:floor(N_X*0.33), :), Y_train{1}(floor(N_X*0.33)+1:floor(N_X*0.66), :), Y_train{1}(floor(N_X*0.66)+1:end, :)};
end

% Auxiliary variables
N_samples = length(Y);
N_in = size(X{1}, 2);
N_out = size(Y{1}, 2);
    
% Compute the K-Fold partitions
c = cvpartition(N_samples, 'kfold', 3);
N_comb = size(params, 2);
best_err = Inf;
best_params = [];

for ii = 1:N_comb
    
    curr_params = params(:, ii);

    statusbar(0, 'Processing %i of %i', ii, N_comb);
    
    % Construct the ESN
    esn = generate_esn(N_in, curr_params(1), N_out, ...
        'spectralRadius', curr_params(2), 'inputScaling', repmat(curr_params(4), N_in, 1), ...
        'teacherScaling', curr_params(5), 'feedbackScaling', curr_params(3), ...
        'noiseLevel', curr_params(7), 'connectivity', 0.25);
    
    err = 0;
    
    % Train and test for 3 folds
    for jj = 1:3

        trainInputSequence = X(c.training(jj));
        trainOutputSequence = Y(c.training(jj));
        testInputSequence = X(c.test(jj));
        testOutputSequence = Y(c.test(jj));

        stateCollection = gather_states(trainInputSequence, trainOutputSequence, esn, config.dropout);
        teacherCollection = gather_teacher(trainOutputSequence, esn, config.dropout, true);
        
        esn.outputWeights = (stateCollection'*stateCollection + curr_params(6)*eye(N_in + curr_params(1)))\(stateCollection'*teacherCollection);
        esn.outputWeights = esn.outputWeights';
        
        err = err + test_esn(testInputSequence, testOutputSequence, esn, config);
        
    end
    
    err = err/3;
    
    % Check if we have a new minimum
    if(err < best_err)
        fprintf('Tested N_res = %i, rho = %.1f, inputScaling = %.1f, teacherScaling = %.1f, \n\t feedbackScaling = %.1f, noiseLevel = %f, lambda = %f...\n', ...
        curr_params(1), curr_params(2), curr_params(4), curr_params(5), curr_params(3), curr_params(7), curr_params(6));
        fprintf('Final validation error is %.2f.\n', err);
        cprintf('_comment', 'New best!\n');
        best_err = err;
        best_params = curr_params;
    end
    
end

% Print the optimal parameters on screen
cprintf('*text', 'Best parameters are N_res = %i, rho = %.1f, inputScaling = %.1f, teacherScaling = %.1f, \n\t feedbackScaling = %.1f, noiseLevel = %f, lambda = %f...\n', ...
        best_params(1), best_params(2), best_params(4), best_params(5), best_params(3), best_params(7), best_params(6));
    